<?php

namespace App\Http\Responses;

use GuzzleHttp\Psr7\UploadedFile;
use Illuminate\Contracts\Support\Responsable;
use phpDocumentor\Reflection\Types\Collection;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\HttpFoundation\Response;

class ApiResponse implements Responsable
{
    private $data;
    private $status;

    /**
     * @param $data
     * @param int $status
     */
    public function __construct($data, $status = 200)
    {
        $this->data = $data;
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function success(): bool
    {
        return $this->status === 200;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request): Response
    {
        $respArray = ['success' => $this->success()];
        foreach ($this->data as $key => $data){
            $respArray[$key] = $data;
        }
        return response()->json($respArray, $this->status)->withHeaders(['Content-Type' => 'application/json', 'Accept'=>'application/json']);
    }
}
