<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\LoginException;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Responses\ApiResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /**
     * @param RegisterRequest $request
     * @return ApiResponse
     */
    public function register(RegisterRequest $request): ApiResponse
    {
        $token = Str::random(60);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return new ApiResponse([], 200);
    }

    /**
     * @param Request $request
     * @return ApiResponse
     * @throws LoginException
     */
    public function login(LoginRequest $request): ApiResponse
    {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            throw new LoginException();
        }

        return new ApiResponse(['token' => $token]);
    }

    /**
     * @return ApiResponse
     */
    public function logout(User $user): ApiResponse
    {
        auth()->logout();
        return new ApiResponse([], 200);
    }

}
