<?php

namespace App\Http\Controllers\Api;

use App\DataTransferObjects\TodoData;
use App\DataTransferObjects\TodoDataCollection;
use App\Http\Requests\CreateTodoRequest;
use App\Http\Requests\DeleteTodoRequest;
use App\Http\Responses\ApiResponse;
use App\Models\Todo;
use App\TodoService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{

    /**
     * @var TodoService
     */
    private TodoService $todoService;

    /**
     * @param TodoService $todoService
     */
    public function __construct(TodoService $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * @return ApiResponse
     */
    public function index(): ApiResponse
    {
        $user = Auth::user();
        return new ApiResponse(['list' => new TodoDataCollection($user->todos->all())], 200);
    }

    /**
     * @param CreateTodoRequest $request
     * @return ApiResponse
     */
    public function create(CreateTodoRequest $request): ApiResponse
    {
        $user = Auth::user();
        $todo = new Todo($request->validated());
        $todo->user()->associate($user);
        $todo->save();
        return new ApiResponse(new TodoDataCollection($user->todos->all()), 200);
    }

    /**
     * @param DeleteTodoRequest $request
     * @return ApiResponse
     * @throws AuthorizationException
     */
    public function delete(DeleteTodoRequest $request)
    {
        $user = Auth::user();
        $todo = Todo::find($request->get("taskID"));
        if ($user->can('delete', $todo)) {
            $todo->delete();
            return new ApiResponse([]);
        }
        throw new AuthorizationException();
    }

    /**
     * @param Request $request
     * @return ApiResponse
     */
    public function exportCSV(Request $request)
    {
        $user = Auth::user();
        $todoDataCollection = new TodoDataCollection($user->todos->all());
        $csv = $this->todoService->makeCSV($todoDataCollection, $user);

        return new ApiResponse(["csv" => $csv]);
    }
}
