<?php

namespace App\DataTransferObjects;

use Illuminate\Support\Collection;

class TodoDataCollection extends Collection
{
    /**
     * @param array $data
     */
    public function __construct(array $data = []){
        parent::__construct();
        foreach ($data as $d){
            $this->items[]= TodoData::fromModel($d);
        }
    }

}
