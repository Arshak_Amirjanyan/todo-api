<?php

namespace App\Exceptions;

use App\Http\Responses\ApiResponse;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Exception;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Exception\NameException;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
      $this->renderable(function (BadRequestException $e, $request) {
          return new ApiResponse(['error' => 'Bad Request. make sure Content-Type and Accept are set to application/json'], 404);
      });
        $this->renderable(function (ModelNotFoundException $e, $request) {
            return new ApiResponse(['error' => $e->getMessage()], 404);
        });
        $this->renderable(function (ResourceNotFoundException $e, $request) {
            return new ApiResponse(['error' => $e->getMessage()], 404);
        });
        $this->renderable(function (ValidationException $e, $request) {
            return new ApiResponse(['error' => $e->errors()], 422);
        });
        $this->renderable(function (AuthenticationException $e, $request) {
            return new ApiResponse(['error' => 'invalid token'], 401);
        });
        $this->renderable(function (AuthorizationException $e, $request) {
            return new ApiResponse(['error' => 'you dont have access'], 403);
        });
        $this->renderable(function (Exception $e, $request) {
            return new ApiResponse(['error' => $e->getMessage()], 500);
        });
    }
}
