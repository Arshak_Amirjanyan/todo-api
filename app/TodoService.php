<?php

namespace App;

use App\DataTransferObjects\TodoDataCollection;
use App\Models\User;

class TodoService
{
    /**
     * @param TodoDataCollection $todoDataCollection
     * @param User $user
     * @return String
     */
    public function makeCSV(TodoDataCollection $todoDataCollection, User $user): String
    {
        $fileName = 'todos'.$user->id.'.csv';
        $path = 'storage/'.$fileName;
        $fp = fopen($path, 'w');
        foreach ($todoDataCollection as $item){
            fputcsv($fp, $item->toArray());
        }

        fclose($fp);

        return url($path);

        //The method will return the contents of the file if this line is used
        //return file_get_contents($path);
    }
}
