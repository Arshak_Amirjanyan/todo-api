<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->postJson('/api/login', ['email' => 'vagho@mail.ru', 'password' => '123456']);
        $response->assertStatus(200)->assertJson(['success' => true]);

        $response->assertStatus(200);
    }
}
